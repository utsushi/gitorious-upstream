# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR SEIKO EPSON CORPORATION
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: utsushi 0.6.0\n"
"Report-Msgid-Bugs-To: linux-scanner@epson.jp\n"
"POT-Creation-Date: 2014-04-25 08:59+0900\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: connexions/usb.cpp:96
msgid "unable to initialise USB support"
msgstr ""

#: connexions/usb.cpp:113 connexions/usb.cpp:359
msgid "no usable, matching device"
msgstr ""

#: drivers/esci/compound-scanner.cpp:352
msgid "Loading completed"
msgstr ""

#: drivers/esci/compound-scanner.cpp:353
msgid "Ejecting completed"
msgstr ""

#: drivers/esci/compound-scanner.cpp:377
msgid "Cleaning is complete."
msgstr ""

#: drivers/esci/compound-scanner.cpp:378
msgid "Calibration is complete."
msgstr ""

#: drivers/esci/compound-scanner.cpp:386 drivers/esci/compound-scanner.cpp:411
msgid "Cleaning is failed."
msgstr ""

#: drivers/esci/compound-scanner.cpp:387 drivers/esci/compound-scanner.cpp:413
msgid "Calibration is failed."
msgstr ""

#: drivers/esci/compound-scanner.cpp:407
msgid "Loading failed"
msgstr ""

#: drivers/esci/compound-scanner.cpp:409
msgid "Ejecting failed"
msgstr ""

#: drivers/esci/compound-scanner.cpp:415
msgid "Maintenance failed"
msgstr ""

#: drivers/esci/compound-scanner.cpp:524 drivers/esci/extended-scanner.cpp:203
msgid "Document Source"
msgstr ""

#: drivers/esci/compound-scanner.cpp:538
msgid "Image Type"
msgstr ""

#: drivers/esci/compound-scanner.cpp:551
msgid "Dropout"
msgstr ""

#: drivers/esci/compound-scanner.cpp:564
msgid "Transfer Format"
msgstr ""

#: drivers/esci/compound-scanner.cpp:565
msgid ""
"Selecting a compressed format such as JPEG normally results in faster device "
"side processing."
msgstr ""

#: drivers/esci/compound-scanner.cpp:579
msgid "JPEG Quality"
msgstr ""

#: drivers/esci/compound-scanner.cpp:592 filters/threshold.cpp:49
msgid "Threshold"
msgstr ""

#: drivers/esci/compound-scanner.cpp:605
msgid "Gamma"
msgstr ""

#: drivers/esci/compound-scanner.cpp:622 filters/lut.cpp:129
msgid "Brightness"
msgstr ""

#: drivers/esci/compound-scanner.cpp:629 filters/lut.cpp:138
msgid "Contrast"
msgstr ""

#: drivers/esci/compound-scanner.cpp:643
msgid "Transfer Size"
msgstr ""

#: drivers/esci/compound-scanner.cpp:658
msgid "Border Fill"
msgstr ""

#: drivers/esci/compound-scanner.cpp:674
msgid "Left Border"
msgstr ""

#: drivers/esci/compound-scanner.cpp:679
msgid "Right Border"
msgstr ""

#: drivers/esci/compound-scanner.cpp:684
msgid "Top Border"
msgstr ""

#: drivers/esci/compound-scanner.cpp:689
msgid "Bottom Border"
msgstr ""

#: drivers/esci/compound-scanner.cpp:706
msgid "Force Extent"
msgstr ""

#: drivers/esci/compound-scanner.cpp:707
msgid ""
"Force the image size to equal the user selected size.  Scanners may trim the "
"image data to the detected size of the document.  This may result in images "
"that are not all exactly the same size.  This option makes sure all image "
"sizes match the selected area.\n"
"Note that this option may slow down application/driver side processing."
msgstr ""

#: drivers/esci/compound-scanner.cpp:735
msgid "esci::compound_scanner(): internal inconsistency"
msgstr ""

#: drivers/esci/compound-scanner.cpp:749
msgid "Calibration"
msgstr ""

#: drivers/esci/compound-scanner.cpp:750
msgid "Calibrating ..."
msgstr ""

#: drivers/esci/compound-scanner.cpp:757
msgid "Cleaning"
msgstr ""

#: drivers/esci/compound-scanner.cpp:758
msgid "Cleaning ..."
msgstr ""

#: drivers/esci/compound-scanner.cpp:765
msgid "Eject"
msgstr ""

#: drivers/esci/compound-scanner.cpp:766
msgid "Ejecting ..."
msgstr ""

#: drivers/esci/compound-scanner.cpp:773
msgid "Load"
msgstr ""

#: drivers/esci/compound-scanner.cpp:774
msgid "Loading ..."
msgstr ""

#: drivers/esci/compound-scanner.cpp:1657
#: drivers/esci/grammar-capabilities.cpp:444
msgid "RAW"
msgstr ""

#: drivers/esci/compound-scanner.cpp:1713
#, boost-format
msgid ""
"Scan area too small.\n"
"The area needs to be larger than %1% by %2%."
msgstr ""

#: drivers/esci/compound-scanner.cpp:1992
msgid "Duplex"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2004
msgid "Image Count"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2016
msgid "Detect Double Feed"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2131
#: drivers/esci/compound-scanner.cpp:2201
msgid "Bind X and Y resolutions"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2135
#: drivers/esci/compound-scanner.cpp:2170
#: drivers/esci/compound-scanner.cpp:2205
#: drivers/esci/compound-scanner.cpp:2240 drivers/esci/extended-scanner.cpp:85
msgid "Resolution"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2139
#: drivers/esci/compound-scanner.cpp:2154
#: drivers/esci/compound-scanner.cpp:2209
#: drivers/esci/compound-scanner.cpp:2224
msgid "X Resolution"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2143
#: drivers/esci/compound-scanner.cpp:2158
#: drivers/esci/compound-scanner.cpp:2213
#: drivers/esci/compound-scanner.cpp:2228
msgid "Y Resolution"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2188
msgid "Enable Resampling"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2189
msgid ""
"This option provides the user with a wider range of supported resolutions.  "
"Resolutions not supported by the hardware will be achieved through image "
"processing methods."
msgstr ""

#: drivers/esci/compound-scanner.cpp:2265
msgid "Manual"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2266
msgid "Maximum"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2275
msgid "Scan Area"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2283
#: drivers/esci/extended-scanner.cpp:105
msgid "Top Left X"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2291
#: drivers/esci/extended-scanner.cpp:121
msgid "Top Left Y"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2299
#: drivers/esci/extended-scanner.cpp:113
msgid "Bottom Right X"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2307
#: drivers/esci/extended-scanner.cpp:129
msgid "Bottom Right Y"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2330
msgid "Crop"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2341
msgid "Crop Adjustment"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2365
msgid "Deskew"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2393
msgid "Overscan"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2415
msgid "internal error: no document source"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2793
msgid ""
"Authentication is required.\n"
"Unfortunately, this version of the driver does not support authentication "
"yet."
msgstr ""

#: drivers/esci/compound-scanner.cpp:2797
#, boost-format
msgid "Unknown device error: %1%/%2%"
msgstr ""

#: drivers/esci/compound-scanner.cpp:2887 sane/backend.cpp:279
msgid "Please close the ADF cover and try again."
msgstr ""

#: drivers/esci/compound-scanner.cpp:2889 sane/backend.cpp:272
msgid "Clear the ADF document jam and try again."
msgstr ""

#: drivers/esci/compound-scanner.cpp:2891 sane/backend.cpp:269
msgid "Please put your document in the ADF before scanning."
msgstr ""

#: drivers/esci/compound-scanner.cpp:2893 sane/backend.cpp:275
msgid ""
"A multi page feed occurred in the ADF.\n"
"Clear the document feeder and try again."
msgstr ""

#: drivers/esci/compound-scanner.cpp:2896
msgid ""
"A fatal ADF error has occurred.\n"
"Resolve the error condition and try again.  You may have to restart the scan "
"dialog or application in order to be able to scan."
msgstr ""

#: drivers/esci/compound-scanner.cpp:2910
msgid "A fatal error has occurred"
msgstr ""

#: drivers/esci/compound-tweaks.cpp:111 drivers/esci/compound-tweaks.cpp:189
#: drivers/esci/extended-scanner.cpp:144
msgid "Speed"
msgstr ""

#: drivers/esci/compound-tweaks.cpp:112 drivers/esci/compound-tweaks.cpp:190
msgid "Optimize image acquisition for speed"
msgstr ""

#: drivers/esci/compound.cpp:63
msgid "crossed wires"
msgstr ""

#: drivers/esci/compound.cpp:559
msgid "unknown extension designation"
msgstr ""

#: drivers/esci/compound.cpp:626
msgid ""
"The device is in use.  Please wait until the device becomes available, then "
"try again."
msgstr ""

#: drivers/esci/exception.hpp:63
msgid "invalid parameter"
msgstr ""

#: drivers/esci/exception.hpp:72
msgid "unknown reply"
msgstr ""

#: drivers/esci/exception.hpp:81
msgid "invalid command"
msgstr ""

#: drivers/esci/exception.hpp:90
msgid "device busy"
msgstr ""

#: drivers/esci/exception.hpp:99
msgid "protocol error"
msgstr ""

#: drivers/esci/extended-scanner.cpp:135
msgid "Line Art"
msgstr ""

#: drivers/esci/extended-scanner.cpp:136
msgid "B/W"
msgstr ""

#: drivers/esci/extended-scanner.cpp:137
msgid "Color"
msgstr ""

#: drivers/esci/extended-scanner.cpp:140
msgid "Color Mode"
msgstr ""

#: drivers/esci/extended-scanner.cpp:151
msgid "Line Count"
msgstr ""

#: drivers/esci/extended-scanner.cpp:152
msgid ""
"Specify how many scan lines to move from the device to the software in one "
"transfer.  Note that 0 will use the maximum usable value.  Values larger "
"than the maximum usable value are clamped to the maximum."
msgstr ""

#: drivers/esci/extended-scanner.cpp:171
#: drivers/esci/grammar-capabilities.cpp:316
#: drivers/esci/grammar-capabilities.cpp:326
msgid "Flatbed"
msgstr ""

#: drivers/esci/extended-scanner.cpp:176
msgid "ADF Simplex"
msgstr ""

#: drivers/esci/extended-scanner.cpp:177
msgid "ADF Duplex"
msgstr ""

#: drivers/esci/extended-scanner.cpp:181
#: drivers/esci/grammar-capabilities.cpp:314
#: drivers/esci/grammar-capabilities.cpp:324
msgid "ADF"
msgstr ""

#: drivers/esci/extended-scanner.cpp:190
msgid "Primary TPU"
msgstr ""

#: drivers/esci/extended-scanner.cpp:191
msgid "Secondary TPU"
msgstr ""

#: drivers/esci/extended-scanner.cpp:195
#: drivers/esci/grammar-capabilities.cpp:315
#: drivers/esci/grammar-capabilities.cpp:325
msgid "TPU"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:246
#: drivers/esci/grammar-capabilities.cpp:385
msgid "None"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:255
msgid "White"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:256
msgid "Black"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:352
msgid "Off"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:353
msgid "Normal"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:354
msgid "Sensitive"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:394
msgid "Red (1 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:395
msgid "Red (8 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:396
msgid "Red (16 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:397
msgid "Green (1 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:398
msgid "Green (8 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:399
msgid "Green (16 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:400
msgid "Blue (1 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:401
msgid "Blue (8 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:402
msgid "Blue (16 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:445
msgid "JPEG"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:562
msgid "Color (1 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:563
msgid "Color (8 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:564
msgid "Color (16 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:565
msgid "Gray (1 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:566
msgid "Gray (8 bit)"
msgstr ""

#: drivers/esci/grammar-capabilities.cpp:567
msgid "Gray (16 bit)"
msgstr ""

#: filters/image-skip.cpp:67
msgid "Blank Image Threshold"
msgstr ""

#: filters/jpeg.cpp:231
msgid "Buffer Size"
msgstr ""

#: filters/jpeg.cpp:252
msgid "Image Quality"
msgstr ""

#: filters/lut.cpp:102
msgid "lut filter supports 8 or 16 bit only."
msgstr ""

#: filters/lut.cpp:130
msgid "Change brightness of the acquired image."
msgstr ""

#: filters/lut.cpp:139
msgid "Change contrast of the acquired image."
msgstr ""

#: filters/padding.cpp:102
msgid "padding only works with raster images of known size"
msgstr ""

#: filters/pdf/object.cpp:60
msgid "PDF object number overflow"
msgstr ""

#: filters/pdf/writer.cpp:67
msgid "PDF filter octet count mismatch"
msgstr ""

#: filters/pdf/writer.cpp:80
msgid "invalid call to _pdf_::writer::write (object&)"
msgstr ""

#: filters/pdf/writer.cpp:100
msgid "invalid call to _pdf_::writer::begin_stream ()"
msgstr ""

#: filters/pdf/writer.cpp:125 filters/pdf/writer.cpp:137
msgid "invalid call to _pdf_::writer::write ()"
msgstr ""

#: filters/pdf/writer.cpp:149
msgid "invalid call to _pdf_::writer::end_stream ()"
msgstr ""

#: filters/pdf/writer.cpp:176
msgid "cannot write header in stream mode"
msgstr ""

#: filters/pdf/writer.cpp:189
msgid "cannot write trailer in stream mode"
msgstr ""

#: gtkmm/action-dialog.cpp:117 gtkmm/action-dialog.cpp:124
msgid "Maintenance"
msgstr ""

#: gtkmm/chooser.cpp:71
msgid "No devices found"
msgstr ""

#: gtkmm/chooser.cpp:78
msgid "Select a device"
msgstr ""

#: gtkmm/chooser.cpp:213
#, boost-format
msgid ""
"Cannot access %1%\n"
"(%2%)\n"
"%3%"
msgstr ""

#: gtkmm/dialog-alt.glade:29 gtkmm/dialog-alt.glade:278 gtkmm/dialog.glade:28
#: gtkmm/dialog.glade:345
msgid "_Scan"
msgstr ""

#: gtkmm/dialog-alt.glade:39 gtkmm/dialog.glade:48
msgid "_Details:"
msgstr ""

#: gtkmm/dialog-alt.glade:48
msgid "Manage Scanners ..."
msgstr ""

#: gtkmm/dialog-alt.glade:57
msgid "Restore factory settings"
msgstr ""

#: gtkmm/dialog-alt.glade:67 gtkmm/dialog.glade:85
msgid "Refresh"
msgstr ""

#: gtkmm/dialog-alt.glade:87 gtkmm/dialog.glade:144
msgid "Scanning Dialog"
msgstr ""

#: gtkmm/dialog-alt.glade:123 gtkmm/dialog.glade:182
msgid "Scanner:"
msgstr ""

#: gtkmm/dialog-alt.glade:162
msgid "Presets:"
msgstr ""

#: gtkmm/dialog-alt.glade:192
msgid ""
"<u><b>Settings Description</b></u>\n"
"A short, human readable description of the current settings, especially "
"those without a directly visible effect in the preview area below."
msgstr ""

#: gtkmm/dialog-alt.glade:241 gtkmm/dialog.glade:318
msgid ""
"<u><b>Editor Zone</b></u>\n"
"Displays only those editors that correspond to one of the <u>active</u> tags "
"in the Toggle Zone.\n"
"\n"
"Active tags indicate the kind of settings that are relevant to the user's "
"task at hand.  Controls that only correspond to inactive tags will not be "
"displayed and will not distract and/or confuse the user."
msgstr ""

#: gtkmm/dialog-alt.glade:264
msgid "gtk-cancel"
msgstr ""

#: gtkmm/dialog-alt.glade:271
msgid "gtk-refresh"
msgstr ""

#: gtkmm/dialog.cpp:80
msgid "Dialog specification requires a 'uimanager'"
msgstr ""

#: gtkmm/dialog.cpp:260
msgid "Save As"
msgstr ""

#: gtkmm/dialog.cpp:273
msgid "Image Files"
msgstr ""

#: gtkmm/dialog.cpp:279
msgid "All Files"
msgstr ""

#: gtkmm/dialog.cpp:310
msgid "Unsupported file format."
msgstr ""

#: gtkmm/dialog.cpp:313
#, boost-format
msgid ""
"Support for images in %1% format has not been implemented yet.\n"
"Please use PNM, PNG, JPEG, PDF or TIFF for now."
msgstr ""

#: gtkmm/dialog.cpp:484 gtkmm/dialog.cpp:509 gtkmm/dialog.cpp:553
#: gtkmm/dialog.cpp:605 gtkmm/dialog.cpp:623 src/scan-cli.cpp:848
#: src/scan-cli.cpp:873 src/scan-cli.cpp:917 src/scan-cli.cpp:969
#: src/scan-cli.cpp:985
#, boost-format
msgid "conversion from %1% to %2% is not supported"
msgstr ""

#: gtkmm/dialog.cpp:522 src/scan-cli.cpp:886
msgid "JPEG does not support bi-level imagery"
msgstr ""

#: gtkmm/dialog.cpp:663
#, boost-format
msgid "A file named \"%1%\" already exists.  Do you want to replace it?"
msgstr ""

#: gtkmm/dialog.cpp:669
#, boost-format
msgid ""
"The file already exists in \"%1%\".  Replacing it will overwrite its "
"contents."
msgstr ""

#: gtkmm/dialog.cpp:681
msgid "This may overwrite existing files!"
msgstr ""

#: gtkmm/dialog.cpp:685
#, boost-format
msgid "Files matching \"%1%\" may be overwritten."
msgstr ""

#: gtkmm/dialog.glade:38
msgid "_Maintenance"
msgstr ""

#: gtkmm/dialog.glade:92
msgid "Zoom in"
msgstr ""

#: gtkmm/dialog.glade:99
msgid "Zoom out"
msgstr ""

#: gtkmm/dialog.glade:106
msgid "Actual size"
msgstr ""

#: gtkmm/dialog.glade:113
msgid "Zoom to fit"
msgstr ""

#: gtkmm/dropdown.cpp:170
msgid "To be implemented."
msgstr ""

#: gtkmm/dropdown.cpp:173
#, boost-format
msgid ""
"Support for changing the active item has not been implemented yet.  Should "
"be changing from\n"
"\n"
"\t<b>%1%</b>\n"
"\n"
"to\n"
"\n"
"\t<b>%2%</b>"
msgstr ""

#: gtkmm/dropdown.cpp:193
#, boost-format
msgid ""
"Support for management action functions has not been implemented yet.  This "
"action could manipulate, and revert to,\n"
"\n"
"\t<b>%1%</b>"
msgstr ""

#: gtkmm/editor.cpp:459
msgid "Other"
msgstr ""

#: gtkmm/editor.cpp:470
msgid "Application"
msgstr ""

#: gtkmm/editor.cpp:647
msgid "Restoring previous value"
msgstr ""

#: gtkmm/editor.cpp:650
msgid "The selected combination of values is not supported."
msgstr ""

#: lib/descriptor.cpp:47
msgid "Standard"
msgstr ""

#: lib/descriptor.cpp:48
msgid ""
"If there is any user interface at all, options at the standard level are "
"meant to be made available to the user."
msgstr ""

#: lib/descriptor.cpp:54
msgid "Extended"
msgstr ""

#: lib/descriptor.cpp:55
msgid ""
"Extended options are for those situations where the user needs a bit more "
"control over how things will be done."
msgstr ""

#: lib/descriptor.cpp:61
msgid "Complete"
msgstr ""

#: lib/descriptor.cpp:62
msgid ""
"This is for options that are mostly just relevant for the most demanding of "
"image acquisition jobs or those users will not be satisfied unless they are "
"in complete control."
msgstr ""

#: lib/device.cpp:114
msgid "unhandled state in idevice::read()"
msgstr ""

#: lib/media.cpp:64
msgid "ISO/A3"
msgstr ""

#: lib/media.cpp:65
msgid "ISO/A4"
msgstr ""

#: lib/media.cpp:66
msgid "ISO/A5"
msgstr ""

#: lib/media.cpp:67
msgid "ISO/A6"
msgstr ""

#: lib/media.cpp:69
msgid "JIS/B4"
msgstr ""

#: lib/media.cpp:70
msgid "JIS/B5"
msgstr ""

#: lib/media.cpp:71
msgid "JIS/B6"
msgstr ""

#: lib/media.cpp:73
msgid "Ledger"
msgstr ""

#: lib/media.cpp:74
msgid "Legal"
msgstr ""

#: lib/media.cpp:75
msgid "Letter"
msgstr ""

#: lib/media.cpp:76
msgid "Executive"
msgstr ""

#: lib/option.cpp:595
msgid "cannot add option::map to self"
msgstr ""

#: lib/pump.cpp:83
msgid "Acquire image data asynchronously"
msgstr ""

#: lib/pump.cpp:84
msgid ""
"When true, image acquisition will proceed independently from the rest of the "
"program.  Normally, this would be what you want because it keeps the program "
"responsive to user input and updated with respect to progress.  However, in "
"case of trouble shooting you may want to turn this off to obtain a more "
"predictable program flow.\n"
"Note, you may no longer be able to cancel image acquisition via the normal "
"means when this option is set to false."
msgstr ""

#: lib/pump.cpp:100
msgid "no image data source"
msgstr ""

#: lib/pump.cpp:108
msgid "no output destination"
msgstr ""

#: lib/run-time.cpp:59
msgid "run_time has been initialized already"
msgstr ""

#: lib/run-time.cpp:79
msgid "run_time has not been initialized yet"
msgstr ""

#: lib/run-time.cpp:282
msgid "GNU standard options"
msgstr ""

#: lib/run-time.cpp:283
msgid "Standard options"
msgstr ""

#: lib/run-time.cpp:300
msgid "display this help and exit"
msgstr ""

#: lib/run-time.cpp:301
msgid "output version information and exit"
msgstr ""

#: lib/scanner.cpp:71
msgid "driver not found"
msgstr ""

#: lib/scanner.cpp:150
#, boost-format
msgid "syntax error: invalid UDI '%1%'"
msgstr ""

#: lib/tag.cpp:70
#, boost-format
msgid "Options provided by %1%."
msgstr ""

#: lib/tag.cpp:74
msgid "General"
msgstr ""

#: lib/tag.cpp:75
msgid "Basic options."
msgstr ""

#: lib/tag.cpp:79
msgid "Geometry"
msgstr ""

#: lib/tag.cpp:80
msgid "Scan area and image size related options."
msgstr ""

#: lib/tag.cpp:84
msgid "Enhancement"
msgstr ""

#: lib/tag.cpp:85
msgid "Image modification options."
msgstr ""

#: outputs/tiff.cpp:70
msgid "failure writing TIFF scanline"
msgstr ""

#: outputs/tiff.cpp:129
msgid "unsupported colour space"
msgstr ""

#: outputs/tiff.cpp:134
msgid "unsupported bit depth"
msgstr ""

#: outputs/tiff.cpp:164
msgid "failure writing TIFF directory"
msgstr ""

#: sane/backend.cpp:1011
msgid ""
"The current locale settings are not supported by the standard C++ library "
"used by this application.  This is most likely caused by a misconfigured "
"locale but may also be due to use of a C++ library without localization "
"support.  You can work around this issue by starting the application in a \"C"
"\" locale, but you really should check your locale configuration and the "
"locale support of the C++ library used by the application."
msgstr ""

#: sane/backend.cpp:1035
msgid "library initialization failed"
msgstr ""

#: sane/backend.hpp:293
#, c-format, boost-format
msgid "Unknown SANE status code %d"
msgstr ""

#: sane/handle.cpp:1135
msgid ""
"SANE API specification violation\n"
"The option number count has to be the first option."
msgstr ""

#: sane/handle.cpp:1307
msgid ""
"SANE API specification violation\n"
"Option names must start with a lower-case ASCII character."
msgstr ""

#: sane/value.cpp:73
msgid "internal inconsistency"
msgstr ""

#: sane/value.cpp:221
msgid "value type does not support multiplication"
msgstr ""

#: sane/value.cpp:245
msgid "value type does not support division"
msgstr ""

#: src/help.cpp:52
msgid "display help information for a command"
msgstr ""

#: src/list.cpp:51 src/main.cpp:66
msgid "list available image acquisition devices"
msgstr ""

#: src/main.cpp:61
msgid "Supported commands"
msgstr ""

#: src/main.cpp:64
msgid "display the help for a command and exit"
msgstr ""

#: src/main.cpp:65
msgid "output command version information and exit"
msgstr ""

#: src/main.cpp:67
msgid "scan with a suitable utility"
msgstr ""

#: src/main.cpp:72
msgid "next generation image acquisition"
msgstr ""

#: src/scan-cli.cpp:154
msgid "device found but has no driver"
msgstr ""

#: src/scan-cli.cpp:169
#, boost-format
msgid "cannot find '%1%'"
msgstr ""

#: src/scan-cli.cpp:174
msgid "no devices available"
msgstr ""

#: src/scan-cli.cpp:189
#, boost-format
msgid "%1%: not supported"
msgstr ""

#: src/scan-cli.cpp:234
#, boost-format
msgid ""
"%1%\n"
"Allowed values: %2%"
msgstr ""

#: src/scan-cli.cpp:241
#, boost-format
msgid "Allowed values: %1%"
msgstr ""

#: src/scan-cli.cpp:410
#, boost-format
msgid ""
"option parser internal inconsistency\n"
"  key = %1%"
msgstr ""

#: src/scan-cli.cpp:496
msgid "image acquisition device to use"
msgstr ""

#: src/scan-cli.cpp:498
msgid "output destination to use"
msgstr ""

#: src/scan-cli.cpp:512 src/scan-gtkmm.cpp:73
msgid "Utility options"
msgstr ""

#: src/scan-cli.cpp:515
msgid "log device I/O in hexdump format"
msgstr ""

#: src/scan-cli.cpp:517
msgid ""
"output image format\n"
"PNM, PNG, JPEG, PDF, TIFF or one of the device supported transfer-formats.  "
"The explicitly mentioned types are normally inferred from the output file "
"name."
msgstr ""

#: src/scan-cli.cpp:566
msgid "Device actions"
msgstr ""

#: src/scan-cli.cpp:576
msgid ""
"Only perform the actions given on the command-line.  Do not perform image "
"acquisition."
msgstr ""

#: src/scan-cli.cpp:582
msgid "Device options"
msgstr ""

#: src/scan-cli.cpp:594
msgid "Add-on options"
msgstr ""

#: src/scan-cli.cpp:610
msgid ""
"Note: device options may be ignored if their prerequisites are not "
"satisfied.\n"
"A '--duplex' option may be ignored if you do not select the ADF, for "
"example.\n"
msgstr ""

#: src/scan-gtkmm.cpp:77
msgid "use an alternative GUI layout definition file"
msgstr ""

#: src/scan-gtkmm.cpp:79
msgid "use an alternative GUI resource file"
msgstr ""

#: src/scan.cpp:77
msgid "Command options"
msgstr ""

#: src/scan.cpp:82
msgid ""
"Start an interactive user interface\n"
"The default behavior depends on the environment where one runs the command.  "
"A scan utility suitable for non-interactive use can be selected with the '--"
"no-interface' option."
msgstr ""

#: src/scan.cpp:91
msgid "acquire images with a suitable utility"
msgstr ""

#: src/version.cpp:52
msgid "display version information for a command"
msgstr ""
